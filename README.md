# Laravel Project

Small laravel project with a fully functional REST API

## Pre-requisites

- mySql database exists 
- update `.env `with database connection details

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
- php.ini has the mysql pdo extensions uncommented 

(Windows)
`extension=php_pdo_mysql.dll`

(Linux)
`extension=pdo_mysql`

## Getting Started
- Run DB Migrations --> `php artisan migrate`
- Run DB Seed --> `php artisan db:seed`
- Start development server --> `php artisan serve`

## Running the tests

`composer test`


